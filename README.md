# Debian Jessie - GNU Radio
&nbsp;

GNU Radio with UHD, and gr-osmosdr via PyBOMBS Dockerfile

&nbsp;
## Pull the image

The image can be pulled with the following command.

&nbsp;

    docker pull registry.gitlab.com/finding-ray/debian-gnuradio:latest

&nbsp;
## Run the image without GUI

Use the following command

&nbsp;

    docker run --rm -ti registry.gitlab.com/finding-ray/debian-gnuradio:latest


&nbsp;
## Run the image with X forwarding

Use the following command to run on a Linux X server machine with GNU Radio Companion accessible.

    docker run --rm -ti -e DISPLAY -v $HOME/.Xauthority:/root/.Xauthority \
        --net=host registry.gitlab.com/finding-ray/debian-gnuradio:latest

&nbsp;
## Build flow

&nbsp;

    gnuradio-build-deps -> gnuradio-pybombs -> gnuradio-deps -> gnuradio

&nbsp;

*   `gnuradio-build-deps` provides the dependencies for PyBOMBS and GNU Radio
*   `gnuradio-pybombs` installs and configures PyBOMBS
*   `gnuradio-deps` installs the dependencies for GNU Radio and UHD only available through PyBOMBS
*   `gnuradio` installs GNU Radio and all the hardware dependencies

