FROM debian

RUN apt-get --yes update
RUN apt-get --yes install apt-utils \
                          python-pip \
                          autotools-dev \
                          autoconf \
                          cmake \
                          libboost-all-dev \
                          libcppunit-dev \
                          swig \
                          doxygen \
                          liblog4cpp5 \
                          python-scipy \
                          libffi-dev \
                          libssl-dev \
                          git \
                          sudo \
                          libssl-dev \
                          libtiff5-dev \
                          libxml2-dev \
                          libxslt1-dev && \
    rm -rf /var/lib/{apt,dpkg,cache,log}

RUN pip install pybombs
RUN pybombs -v recipes add gr-recipes git+https://github.com/gnuradio/gr-recipes.git && \
    pybombs -v recipes add gr-etcetera git+https://github.com/gnuradio/gr-etcetera.git && \
    mkdir -p ~/prefix/

ENV depends "wxpython gqrx doxygen python-requests libusb boost mako fftw cppunit swig gsl uhd alsa cheetah wxpython numpy lxml pygtk pycairo cmake pyqt4 pyqwt5 apache-thrift liblog4cpp zeromq python-zmq git"

RUN pybombs -v prefix init -a default ~/prefix/default/
RUN pybombs -v install ${depends}
RUN pybombs -v install uhd
RUN pybombs -v install gnuradio
RUN pybombs -v install gr-osmosdr

RUN pybombs -v install gr-gsm
RUN pybombs -v install gr-lte
RUN pybombs -v install gr-cdma

RUN mkdir -p ~/.gnuradio
RUN echo [grc] >> ~/.gnuradio/config.conf && \
    echo local_blocks_path=/home/username/prefix/default/src/gnuradio/grc/blocks/ >> ~/.gnuradio/config.conf
