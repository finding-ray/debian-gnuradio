FROM debian

# Install CPP Dependencies
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
        apt-get --yes install --no-install-recommends \
            libasound2 \
            libcomedi0 \
            libfftw3-bin \
            libportaudio2 \
            libsdl-image1.2 \
            libuhd003 \
            libusb-1.0-0 \
            pkg-config \
    && apt-get clean \
    && rm -rf /var/lib/{apt,dpkg,cache,log}

# Install Python Dependencies
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
        apt-get --yes install --no-install-recommends \
            python-cheetah \
            python-dev \
            python-gtk2 \
            python-lxml \
            python-numpy \
            python-opengl \
            python-qt4 \
            python-wxgtk3.0 \
            python-zmq \
    && apt-get clean \
    && rm -rf /var/lib/{apt,dpkg,cache,log}

# Install Build Dependencies 
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
        apt-get --yes install --no-install-recommends \
            build-essential \
            ccache \
            apt-utils \
            autotools-dev \
            autoconf \
            cmake \
            libboost-date-time-dev \
            libboost-dev \
            libboost-filesystem-dev \
            libboost-program-options-dev \
            libboost-regex-dev \
            libboost-system-dev \
            libboost-test-dev \
            libboost-thread-dev \
            libcomedi-dev \
            libcppunit-dev \
            libfftw3-dev \
            libgsl0-dev \
            libqt4-dev \
            libqwt-dev \
            libsdl1.2-dev \ 
            libuhd-dev \ 
            libusb-dev \ 
            libusb-1.0-0-dev \ 
            libzmq3-dev \ 
            libssl-dev \
            libtiff5-dev \
            libxml2-dev \
            libxslt1-dev \
            libffi-dev \
            portaudio19-dev \
            pyqt4-dev-tools \
            python-cheetah \
            python-scipy \
            swig \
            git \
            sudo \
    && apt-get clean \
    && rm -rf /var/lib/{apt,dpkg,cache,log}
